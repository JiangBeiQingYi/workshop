package com.beone.admin.config;

import org.springframework.cache.ehcache.EhCacheFactoryBean;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.core.userdetails.cache.EhCacheBasedUserCache;

/**
 * @title
 * @Author 覃球球
 * @Version 1.0 on 2018/4/28.
 * @Copyright 长笛龙吟
 */
@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE)
public class EhCacheConfig {
    @Bean
    public EhCacheManagerFactoryBean getEhCacheManagerFactoryBean(){
        return new EhCacheManagerFactoryBean();
    }

    @Bean
    public EhCacheFactoryBean getEhCacheFactoryBean(){
        EhCacheFactoryBean bean = new EhCacheFactoryBean();
//        bean.setBeanName("userCache");
        bean.setCacheName("userCache");
        bean.setCacheManager(getEhCacheManagerFactoryBean().getObject());
        return bean;
    }

    @Bean(name = "userCache")
    public EhCacheBasedUserCache getUserCache(){
        EhCacheBasedUserCache userCache = new EhCacheBasedUserCache();
        userCache.setCache(getEhCacheFactoryBean().getObject());
        return userCache;
    }
}
