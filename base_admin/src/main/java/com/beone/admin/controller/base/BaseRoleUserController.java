package com.beone.admin.controller.base;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * @Title 运维数据_用户对应的角色 前端控制器
 * @Author 覃球球
 * @Version 1.0 on 2018-01-25
 * @Copyright 长笛龙吟
 */
@Controller
@RequestMapping("/system/operationRoleUser")
public class BaseRoleUserController {

}

