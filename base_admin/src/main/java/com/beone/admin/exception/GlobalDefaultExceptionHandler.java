package com.beone.admin.exception;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * @title  Controller 层异常处理
 * @Author 覃球球
 * @Version 1.0 on 2018/1/31.
 * @Copyright 长笛龙吟
 */
@ControllerAdvice
public class GlobalDefaultExceptionHandler {
    public static final String DEFAULT_ERROR_VIEW = "forward:/error/500";

    private Logger logger = LoggerFactory.getLogger(GlobalDefaultExceptionHandler.class);

    /**
     * 拦截 RestController   ResponseBody Method 异常
     * @param request
     * @param ex
     * @return
     */
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<?> handleRestException(HttpServletRequest request, Throwable ex){
        logger.error("requestUrl {}  异常 {}", request.getRequestURL(), ex);

        HttpStatus status = getStatus(request);
        JSONObject object = new JSONObject();
        object.put("desc",ex.getMessage());
        object.put("url",request.getRequestURL());
        object.put("status", status.value());
        object.put("code",  "fail");
        return new ResponseEntity<JSONObject>(object, status);
    }

    private HttpStatus getStatus(HttpServletRequest request) {
        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        if (statusCode == null) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.valueOf(statusCode);
    }

    /**
     * 拦截 Mvc 页面跳转异常
     *
     *        public String  testView() throws MvcException {
                 try {
                    System.out.println(1/0);
                 }catch (Exception e){
                   throw new MvcException(e.getMessage());
                 }
                 return "viewName";
              }
     * @param request
     * @param e
     * @return
     * @throws Exception
     */
    @ExceptionHandler(MvcException.class)
    public ModelAndView handleMvcException(HttpServletRequest request, Exception e) throws Exception{
        logger.error("requestUrl {}  异常 {}", request.getRequestURL(), e);

        if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null) {
            throw e;
        }

        // Otherwise setup and send the user to a default error-view.
        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", e);
        mav.addObject("url", request.getRequestURL());
        mav.setViewName(DEFAULT_ERROR_VIEW);
        return mav;
    }

}
