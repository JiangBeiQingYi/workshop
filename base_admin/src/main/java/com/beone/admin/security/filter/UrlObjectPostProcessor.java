package com.beone.admin.security.filter;

import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;

/**
 * @title  Url 自定义Security拦截器
 * @Author 覃球球
 * @Version 1.0 on 2017/12/20.
 * @Copyright 长笛龙吟
 */
public class UrlObjectPostProcessor implements ObjectPostProcessor<FilterSecurityInterceptor> {


    public UrlObjectPostProcessor() {
        super();
    }

    private FilterInvocationSecurityMetadataSource securityMetadataSource;

    private AccessDecisionManager accessDecisionManager;

    private AuthenticationManager authenticationManager;

    public void setSecurityMetadataSource(FilterInvocationSecurityMetadataSource securityMetadataSource) {
        this.securityMetadataSource = securityMetadataSource;
    }

    public void setAccessDecisionManager(AccessDecisionManager accessDecisionManager) {
        this.accessDecisionManager = accessDecisionManager;
    }

    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    public <O extends FilterSecurityInterceptor> O postProcess(O fsi) {
        fsi.setSecurityMetadataSource(securityMetadataSource);
        fsi.setAccessDecisionManager(accessDecisionManager);
        fsi.setAuthenticationManager(authenticationManager);
        return fsi;
    }
}
