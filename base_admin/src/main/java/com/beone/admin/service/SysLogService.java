package com.beone.admin.service;

import com.base.ISuperService;
import com.beone.admin.entity.SysLog;
import com.beone.admin.utils.PaginationGatagridTable;

/**
 * @Title 日志管理 服务类
 * @Author 覃球球
 * @Version 1.0 on 2018-10-22
 * @Copyright 贝旺科权
 */
public interface SysLogService extends ISuperService<SysLog> {
    /**
     * 分页显示后台日志列表
     * @param startTime
     * @param endTime
     * @param currPage  当前页码
     * @param pageSize  每页显示记录数
     * @return
     */
    PaginationGatagridTable getSysLogPagination(String startTime, String endTime, int currPage, int pageSize);
}
